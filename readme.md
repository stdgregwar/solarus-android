# Solarus Android

This repo contains a AndroidStudio/gradle project building solarus and its dependencies with ndkBuild and make a SDLActity based appplication.

> **_NOTE:_**  Clone this repo with the `--recursive` git option to pull dependencies

## Things that works

* Video
* Audio
* Quest data loading
* Save files, write directory
* plain lua
* Touch and Bluetooth joystick
* Quest browsing and thumbnails
* LuaJIT

## Things that don't works

* Some UI parts are broken, missing

## Building

Simplest way to build is to use android studio. You may succeed using gradlew directly. But this was not tested. All dependencies are already included in the jni folder.
