package org.solarus_games.solarus;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.preference.DialogPreference;
import android.support.v7.preference.PreferenceDialogFragmentCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.codekidlabs.storagechooser.StorageChooser;

import static android.app.Activity.RESULT_OK;

public class FolderPreferenceFragmentCompat extends PreferenceDialogFragmentCompat {

    private EditText mEditText;
    private final int CHOOSE_FOLDER_INTENT = 123;
    private static final String TAG = "FolderPrefs";

    public static FolderPreferenceFragmentCompat newInstance(
            String key) {
        final FolderPreferenceFragmentCompat
                fragment = new FolderPreferenceFragmentCompat();
        final Bundle b = new Bundle(1);
        b.putString(ARG_KEY, key);
        fragment.setArguments(b);

        return fragment;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);

        mEditText = view.findViewById(R.id.folder_input);

        final StorageChooser chooser = new StorageChooser.Builder()
                .withActivity(getActivity())
                .allowCustomPath(true)
                .withFragmentManager(getActivity().getFragmentManager())
                .setType(StorageChooser.DIRECTORY_CHOOSER)
                .build();

// Show dialog whenever you want by


// get path that the user has chosen
        chooser.setOnSelectListener(new StorageChooser.OnSelectListener() {
            @Override
            public void onSelect(String path) {
                mEditText.setText(path);
                Log.e("SELECTED_PATH", path);
            }
        });


        Button browse = view.findViewById(R.id.browse_button);
        browse.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                chooser.show();
            }
        });

        FolderPreference prefs = (FolderPreference) getPreference();
        mEditText.setText(prefs.getPath());
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {

            String path = mEditText.getText().toString();

            // Get the related Preference and save the value
            DialogPreference preference = getPreference();
            if (preference instanceof FolderPreference) {
                FolderPreference folderPreference =
                        ((FolderPreference) preference);
                // This allows the client to ignore the user value.
                if (folderPreference.callChangeListener(
                        path)) {
                    // Save the value
                    folderPreference.setPath(path);
                }
            }
        } else {
            Log.e(TAG,"DISMISSED");
        }
    }
}
