package org.solarus_games.solarus;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class QuestFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private List<Quest> mQuests;
    private static final String QUESTS_PATHS_KEY = "QUEST_LIST";
    private static final String TAG = "QuestFragment";
    private RecyclerView.Adapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public QuestFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static QuestFragment newInstance(int columnCount) {
        QuestFragment fragment = new QuestFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }

    public void notifyDataSetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quest_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            String quests_folder = getDefaultSharedPreferences(getContext()).getString("quest_folder","");

            // Scan for Solarus Games
            mQuests = scanForSolarusQuest(quests_folder, 1);

            Collections.sort(mQuests);
            mAdapter = new MyQuestRecyclerViewAdapter(mQuests, mListener);
            ViewCompat.setNestedScrollingEnabled(recyclerView,false);
            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onQuestDetailClick(Quest quest);
    }

    private List<Quest> scanForSolarusQuest(String folderName, int depth) {
        File folder = new File(folderName);
        ArrayList<Quest> list = new ArrayList<>();
        if(folder.exists() && folder.isDirectory()) {
            for (File f : folder.listFiles()) {
                if (isASolarusQuest(f)) {
                    list.add(Quest.fromPath(f.getAbsolutePath()));
                } else if (f.isDirectory() && depth >= 0) {
                    list.addAll(scanForSolarusQuest(f.getAbsolutePath(), depth - 1));
                }
            }
        }
        return list;
    }

    private boolean isASolarusQuest(File fileToTest) {
        // A Solarus games is :
        // - a zip file named game_file.solarus
        // - a folder containing ./data/quest.dat
        if (!fileToTest.isDirectory() && fileToTest.getAbsolutePath().endsWith(".solarus")) {
            return true;
        }

        File dataFolder = new File(fileToTest, "data");
        if (dataFolder.exists()) {
            if (dataFolder.isDirectory()) {
                File questFile = new File(dataFolder, "quest.dat");
                return questFile.exists();
            }
        }
        return false;
    }
}
