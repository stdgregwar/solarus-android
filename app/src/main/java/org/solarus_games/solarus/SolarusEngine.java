package org.solarus_games.solarus;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.PixelCopy;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.libsdl.app.SDLActivity;
import org.solarus_games.solarus.joypad.ButtonMappingManager;
import org.solarus_games.solarus.joypad.VirtualButton;

import java.util.ArrayList;

public class SolarusEngine extends SDLActivity {
    public Quest quest;
    final private String TAG = "SolarusEngine";
    public Bitmap screenCapture;
    private ButtonMappingManager.InputLayout inputLayout;
    private RelativeLayout mButtonLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        quest = Quest.fromPath(getIntent().getExtras().getString("quest_path"));
        SolarusApp.setCurrentQuest(this);


        // Screen orientation
        if (SettingsManager.isForcedLandscape()) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        mButtonLayout = new RelativeLayout(this);
        mLayout.addView(mButtonLayout);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void bringMainActivityToFront() {
        Intent i = new Intent(getApplicationContext(), Solarus.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(i);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        // Ignore certain special keys so they're handled by Android
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            View sv = mSurface;
            int width = sv.getWidth();
            int height = sv.getHeight();
            final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            if(Build.VERSION.SDK_INT >= 24) {
                PixelCopy.request(mSurface, bitmap, new PixelCopy.OnPixelCopyFinishedListener() {
                    @Override
                    public void onPixelCopyFinished(int i) {
                        screenCapture = bitmap;
                        bringMainActivityToFront();
                    }
                }, new Handler(getMainLooper()));
            } else {
                screenCapture = bitmap; //Anyway... should be black
                bringMainActivityToFront();
            }
            return true; //Let the back button go up
        }
        return super.dispatchKeyEvent(event);
    }

    public void exit() {
        SDLActivity.mExitCalledFromJava = true;
        SDLActivity.nativeQuit();

        // Now wait for the SDL thread to quit
        if (SDLActivity.mSDLThread != null) {
            try {
                SDLActivity.mSDLThread.join();
            } catch(Exception e) {
                Log.v(TAG, "Problem stopping thread: " + e);
            }
            SDLActivity.mSDLThread = null;
        }

        SolarusApp.engineDown(this);
        finish();
    }

    @Override
    protected String[] getArguments() {
        ArrayList<String> args = new ArrayList<>();
        args.add("-fullscreen=yes");
        if(!PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean("audio", true)) {
            args.add("-no-audio");
        }
        args.add(quest.path);
        return args.toArray(new String[args.size()]);
    }

    @Override
    public void onResume() {
        super.onResume();

        spawnGamepad();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Replace buttons on screen
        if(SettingsManager.isGamepadEnabled()) {
            updateButtonsPosition();
        }
    }

    private void spawnGamepad() {
        mButtonLayout.removeAllViews();

        if(SettingsManager.isGamepadEnabled()) {
            // Project preferences
            ButtonMappingManager buttonMappingManager = ButtonMappingManager.getInstance(this);

            // Choose the proper InputLayout //TODO don't use only default
            inputLayout = buttonMappingManager.getLayoutById(buttonMappingManager.getDefaultLayoutId());

            // Add buttons
            addButtons();
        }
    }

    /**
     * Draws all buttons.
     */
    private void addButtons() {

        // Adding the buttons
        for (VirtualButton b : inputLayout.getButtonList()) {
            // We add it, if it's not the case already
            if (b.getParent() != mButtonLayout) {
                if (b.getParent() != null) {
                    ((ViewGroup) b.getParent()).removeAllViews();
                }
                mButtonLayout.addView(b);
            }
        }
        updateButtonsPosition();
    }

    public void updateButtonsPosition() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        int screenHeight = getWindowManager().getDefaultDisplay().getHeight();

        for (VirtualButton b : inputLayout.getButtonList()) {
            UiHelper.setLayoutPosition(this, b, b.getPosX(), b.getPosY());

            // We have to adjust the position in portrait configuration
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                params = (RelativeLayout.LayoutParams) b.getLayoutParams();
                // vertical : use approximatively the second part of the screen
                params.topMargin += (int) (screenHeight / 6);
                // horizontal : use a little gap to avoid button to be out of
                // the screen for button to the right
                if (b.getPosX() > 0.5) {
                    params.leftMargin -= screenWidth / 8;
                }

                b.setLayoutParams(params);
            }
        }
    }
}
