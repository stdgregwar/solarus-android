package org.solarus_games.solarus.joypad;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.solarus_games.solarus.R;
import org.solarus_games.solarus.SolarusApp;

import java.util.jar.Attributes;

public class JoypadLayoutPreference extends Preference implements View.OnClickListener {

    private LinearLayout inputLayoutList;
    private ButtonMappingManager buttonMappingManager;
    private PreferenceViewHolder mHolder;

    public JoypadLayoutPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public JoypadLayoutPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setWidgetLayoutResource(R.layout.joypad_layout_preference);
        this.buttonMappingManager = ButtonMappingManager.getInstance(context);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        holder.itemView.setClickable(false); // disable parent click
        View button = holder.findViewById(R.id.add_joypad_layout);
        button.setOnClickListener(this);

        mHolder = holder;
        updateInputLayoutList(holder);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_joypad_layout:
                addAnInputLayout();
                break;
        }
    }

    private void updateInputLayoutList(PreferenceViewHolder holder) {
        if (inputLayoutList == null) {
            inputLayoutList = (LinearLayout) holder.findViewById(R.id.joypad_layouts);
        }
        inputLayoutList.removeAllViews();

        for (ButtonMappingManager.InputLayout i : buttonMappingManager.getLayoutList()) {
            InputLayoutItemListView view = new InputLayoutItemListView(getContext(), i);
            inputLayoutList.addView(view.layout);
        }
    }

    private void refreshInputLayoutList() {
        updateInputLayoutList(mHolder);
    }

    /**
     * Open a dialog box to add an InputLayout
     */
    public void addAnInputLayout() {
        final EditText input = new EditText(getContext());
        // TODO : Restrict the edit text to alpha numeric characters

        // The dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.add_joypad_layout).setView(input)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String text = input.getText().toString();
                        if (!text.isEmpty()) {
                            // Create a new input layout, with the default layout
                            ButtonMappingManager.InputLayout newInputLayout = new ButtonMappingManager.InputLayout(text);
                            newInputLayout.setButtonList(
                                    ButtonMappingManager.InputLayout.getDefaultButtonList(getContext()));

                            // Add it to the input layout list, and open the activity to modify it
                            buttonMappingManager.add(newInputLayout);
                            buttonMappingManager.save();
                            editInputLayout(newInputLayout);

                            refreshInputLayoutList();
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    /**
     * Open a dialog box to configure an InputLayout
     */
    private void configureInputLayout(final ButtonMappingManager.InputLayout gameLayout) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        Context c = SolarusApp.getContext();
        String[] choiceArray = {c.getString(R.string.set_as_default), c.getString(R.string.edit_name),
                c.getString(R.string.edit_layout), c.getString(R.string.delete)};

        builder.setTitle(gameLayout.getName()).setItems(choiceArray, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        buttonMappingManager.setDefaultLayout(gameLayout.getId());
                        buttonMappingManager.save();
                        refreshInputLayoutList();
                        break;
                    case 1:
                        editInputLayoutName(gameLayout);
                        break;
                    case 2:
                        editInputLayout(gameLayout);
                        break;
                    case 3:
                        deleteInputLayout(gameLayout);
                        break;
                    default:
                        break;
                }
            }
        });

        builder.show();
    }

    /**
     * Open a dialog box to configure an InputLayout's name
     */
    private void editInputLayoutName(final ButtonMappingManager.InputLayout inputLayout) {
        // The editText field
        final EditText input = new EditText(getContext());
        input.setText(inputLayout.getName());

        // The dialog box
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.edit_name).setView(input)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String text = input.getText().toString();
                        if (!text.isEmpty()) {
                            inputLayout.setName(text);
                        }
                        buttonMappingManager.save();
                        refreshInputLayoutList();
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    /**
     * Edit an InputLayout by opening the ButtonMapping activity
     */
    private void editInputLayout(final ButtonMappingManager.InputLayout game_layout) {
        Intent intent = new Intent(getContext(), ButtonMappingActivity.class);
        intent.putExtra(ButtonMappingActivity.TAG_ID, game_layout.getId());
        getContext().startActivity(intent);
    }

    /**
     * Delete an InputLayout
     */
    private void deleteInputLayout(final ButtonMappingManager.InputLayout game_layout) {
        // TODO : Ask confirmation
        buttonMappingManager.delete(getContext(), game_layout);
        refreshInputLayoutList();
    }

    private class InputLayoutItemListView {
        private RelativeLayout layout;
        private ImageButton settings_button;

        public InputLayoutItemListView(Context context, final ButtonMappingManager.InputLayout input_layout) {

            LayoutInflater inflater = LayoutInflater.from(context);
            layout = (RelativeLayout) inflater.inflate(R.layout.settings_item_list, null);

            // The name
            TextView input_layout_name = (TextView) layout.findViewById(R.id.controls_settings_preset_name);
            input_layout_name.setText(input_layout.getName());
            if (input_layout.isDefaultInputLayout(buttonMappingManager)) {
                input_layout_name.setText(input_layout_name.getText() + " (" + (R.string.default_layout) + ")");
            }

            // Option button
            settings_button = (ImageButton) layout.findViewById(R.id.controls_settings_preset_option_button);
            settings_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    configureInputLayout(input_layout);
                }
            });

            // Edit the layout by clicking on the view
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editInputLayout(input_layout);
                }
            });
        }
    }
}
