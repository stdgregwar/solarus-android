package org.solarus_games.solarus.joypad;

import org.libsdl.app.SDLActivity;
import org.solarus_games.solarus.SettingsManager;
import org.solarus_games.solarus.UiHelper;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

/**
 * DISCLAIMER. This code is a fork from EasyRPG button system : https://github.com/EasyRPG/Player
 * Credit goes to the authors of these files
 */

public class VirtualButton extends View {
    protected int keyCode;
    protected double posX, posY; // Relative position on the screen
    protected int originalSize, originalLetterSize, resizeFactor, realSize;
    protected String buttonText; // The char displayed on the button
    protected Paint painter;
    protected Rect bound, letterBound = new Rect();
    protected boolean isPressed; // To know when the touch go out the button
    protected boolean debug_mode;
    protected Context context;
    protected Vibrator vibrator;

    public static final int DPAD = -1, ENTER = KeyEvent.KEYCODE_SPACE, CANCEL = KeyEvent.KEYCODE_B,
            SHIFT = KeyEvent.KEYCODE_SHIFT_LEFT, KEY_0 = KeyEvent.KEYCODE_0, KEY_1 = KeyEvent.KEYCODE_1,
            KEY_2 = KeyEvent.KEYCODE_2, KEY_3 = KeyEvent.KEYCODE_3, KEY_4 = KeyEvent.KEYCODE_4,
            KEY_5 = KeyEvent.KEYCODE_5, KEY_6 = KeyEvent.KEYCODE_6, KEY_7 = KeyEvent.KEYCODE_7,
            KEY_8 = KeyEvent.KEYCODE_8, KEY_9 = KeyEvent.KEYCODE_9, KEY_PLUS = 157, KEY_MINUS = 156, KEY_MULTIPLY = 155,
            KEY_DIVIDE = 154, KEY_FAST_FORWARD = KeyEvent.KEYCODE_F;

    public static VirtualButton Create(Context context, int keyCode, double posX, double posY, int size) {
        return new VirtualButton(context, keyCode, posX, posY, size);
    }

    protected VirtualButton(Context context, int keyCode, double posX, double posY, int size) {
        super(context);
        this.context = context;

        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        this.keyCode = keyCode;
        this.posX = posX;
        this.posY = posY;

        this.buttonText = getButtonText(keyCode);

        // Set UI properties
        painter = UiHelper.getUIPainter();

        // Base size: ~1 cm
        originalSize = UiHelper.getPixels(this, 60);
        originalLetterSize = UiHelper.getPixels(this, 25);

        // Retrieve the size factor
        if (SettingsManager.isIgnoreLayoutSizePreferencesEnabled()) {
            this.resizeFactor = SettingsManager.getLayoutSize();
        } else {
            this.resizeFactor = size;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        setProperTransparency(canvas);

        // Draw the circle surrounding the button's letter
        int border = 5;
        canvas.drawCircle(realSize / 2, realSize / 2, realSize / 2 - border, painter);

        // Draw the letter, centered in the circle
        drawCenter(canvas, painter, buttonText);
    }

    protected void setProperTransparency(Canvas canvas) {
        if (!debug_mode) {
            painter.setAlpha(SettingsManager.getLayoutTransparency());
        }
    }

    /** Draw "text" centered in "canvas" */
    protected void drawCenter(Canvas canvas, Paint paint, String text) {
        // Set the text size
        painter.setTextSize(UiHelper.getPixels(this, (int) (originalLetterSize * ((float) resizeFactor / 200))));

        // Draw the text
        Rect bound = new Rect();
        canvas.getClipBounds(bound);
        int cHeight = bound.height();
        int cWidth = bound.width();
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), bound);
        float x = cWidth / 2f - bound.width() / 2f - bound.left;
        float y = cHeight / 2f + bound.height() / 2f - bound.bottom;
        canvas.drawText(text, x, y, paint);
    }

    public int getFuturSize() {
        realSize = (int) ((float) originalSize * resizeFactor / 100);

        return realSize;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int s = getFuturSize();

        setMeasuredDimension(s, s);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (debug_mode) {
            ButtonMappingActivity.dragVirtualButton(this, event);
        } else {
            bound = new Rect(this.getLeft(), this.getTop(), this.getRight(), this.getBottom());

            int action = event.getActionMasked();

            switch (action) {
                case (MotionEvent.ACTION_DOWN):
                    onPressed();
                    return true;
                case (MotionEvent.ACTION_UP):
                    onReleased();
                    return true;
                case (MotionEvent.ACTION_MOVE):
                    if (!bound.contains(this.getLeft() + (int) event.getX(), this.getTop() + (int) event.getY())) {
                        // User moved outside bounds
                        onReleased();
                    }
                    return true;
                default:
                    // return super.onTouchEvent(event);
                    return true;
            }
        }

        return true;
    }

    public void onPressed() {
        if (!debug_mode) {
            if (!isPressed) {
                isPressed = true;

                SDLActivity.onNativeKeyDown(this.keyCode);
                // Vibration
                if (SettingsManager.isVibrationEnabled() && vibrator != null) {
                    vibrator.vibrate(SettingsManager.getVibrationDuration());
                }
            }
        }
    }

    public void onReleased() {
        if (!debug_mode) {
            // We only send a message to SDL Activity if the button is not
            // considered
            // released (in case the touch mouvement go out the button bounds)
            if (isPressed) {
                isPressed = false;
                SDLActivity.onNativeKeyUp(this.keyCode);
            }
        }
    }

    public String getButtonText(int keyCode) {
        String text = ButtonMappingManager.keycodeToString(keyCode);
        if(text.length() > 2) {
            text = text.substring(0,2) + ".";
        }
        return text;
    }

    public double getPosX() {
        return posX;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public double getPosY() {
        return posY;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public String getButtonText() {
        return buttonText;
    }

    public int getSize() {
        return resizeFactor;
    }

    public void setSize(int size) {
        this.resizeFactor = size;
    }

    public void setDebug_mode(boolean debug_mode) {
        this.debug_mode = debug_mode;
    }

}

