package org.solarus_games.solarus;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PausedQuestScreen.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PausedQuestScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PausedQuestScreen extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private OnFragmentInteractionListener mListener;

    private View mMainView;
    private ImageView mQuestScreen;
    private TextView  mQuestTitle;
    private TextView mQuestAuthor;

    public PausedQuestScreen() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PausedQuestScreen.
     */
    // TODO: Rename and change types and number of parameters
    public static PausedQuestScreen newInstance() {
        PausedQuestScreen fragment = new PausedQuestScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
    }

    public void update() {
        SolarusEngine engine = SolarusApp.getCurrentEngine();
        if(SolarusApp.getCurrentQuest() != null) {
            mMainView.setVisibility(View.VISIBLE);
            Quest quest = engine.quest;
            mQuestTitle.setText(quest.title);
            mQuestAuthor.setText(quest.author);
            mQuestScreen.setImageBitmap(engine.screenCapture);
        } else {
            mMainView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_paused_quest_screen, container, false);
        mQuestScreen = v.findViewById(R.id.quest_screen);
        mMainView = v.findViewById(R.id.card_view);
        mQuestAuthor = v.findViewById(R.id.quest_author);
        mQuestTitle = v.findViewById(R.id.quest_title);
        AppCompatImageButton resume_button = v.findViewById(R.id.resume_button);
        AppCompatImageButton quit_button = v.findViewById(R.id.quit_button);

        resume_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onQuestResumeRequest(SolarusApp.getCurrentQuest());
            }
        });

        quit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onQuestQuitRequest(SolarusApp.getCurrentQuest());
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            mListener.attachPausedQuestFragment(this);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onQuestResumeRequest(Quest quest);
        void onQuestQuitRequest(Quest quest);
        void attachPausedQuestFragment(PausedQuestScreen f);
    }
}
