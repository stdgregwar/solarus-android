cmake_minimum_required(VERSION 3.6)

set(ALSOFT_UTILS OFF CACHE BOOL "Shutup" FORCE)
set(ALSOFT_EXAMPLES OFF CACHE BOOL "Shutup" FORCE)
set(ALSOFT_TESTS OFF CACHE BOOL "Shutup" FORCE)

add_subdirectory(openal-soft)
